package com.p2r.my_first_application;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class View_arraylist_Item extends AppCompatActivity {
    TextView tvHeadertex,tvDiscription;
    ImageView ivImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_arraylist_item);
        setRefernce();
        Intent i=getIntent();
        if(i != null){
            tvHeadertex.setText(i.getStringExtra("First_Name"));
            tvDiscription.setText(i.getStringExtra("Discription"));
            ivImage.setImageResource(i.getIntExtra("image",R.drawable.hasti));
        }
    }
    void setRefernce(){
        tvDiscription=findViewById(R.id.tvDiscription);
        tvHeadertex=findViewById(R.id.tvHeaderText);
        ivImage=findViewById(R.id.profile_image);
    }
}